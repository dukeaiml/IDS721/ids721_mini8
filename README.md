# Rust Command-Line Tool for Analyzing Employee Data

Author: Mutian

## Introduction

This Rust command-line tool, named "Salary Analyzer," is designed to analyze employee data stored in CSV files. It provides functionalities to read data from CSV files, process employee information, calculate average salaries, and display salary insights.

## Features

- **CSV Data Parsing**: Utilizes the `csv` crate to parse CSV files and extract employee information, including education, experience, location, job title, age, gender, and salary.

![Alt text](image-3.png)

- **Command-Line Interface (CLI)**: Employs the `clap` crate to provide a user-friendly command-line interface for specifying input CSV files.

- **Salary Analysis**: Calculates the average salary of employees in the dataset and displays the salary information for the last ten employees.

![Alt text](image-2.png)

- **Error Handling**: Implements error handling to gracefully handle invalid CSV data and non-existent file paths.

- **Testing**: Includes unit tests to ensure the correctness and reliability of the tool's functionalities.

## Usage

### Building and Running

1. **Build the Project**: Use `cargo build` to build the project.

2. **Run the Tool**: Execute the tool using `cargo run -- -f <file_path>`, where `<file_path>` is the path to the input CSV file containing employee data.

### Command-Line Arguments

- `-f, --file <FILE>`: Specifies the path to the CSV file containing employee data. If not provided, the tool defaults to "salary_prediction_data.csv."

## Testing

The project includes comprehensive unit tests to verify the correctness and robustness of the tool's functionalities. These tests cover scenarios such as reading invalid CSV data and handling non-existent file paths.

To run the tests, execute `cargo test`.

## Dependencies

The project relies on the following Rust crates:

- `clap`: A command-line argument parsing library for Rust.
- `csv`: A CSV parsing library with support for serde.
- `tempfile`: A library for managing temporary files and directories.

## Conclusion

The "Salary Analyzer" Rust command-line tool provides a convenient and efficient solution for analyzing employee data stored in CSV files. With its user-friendly interface, accurate calculations, and error handling capabilities, it serves as a valuable tool for data analysis tasks.

### Result

![Alt text](image.png)


![Alt text](image-1.png)