use clap::{App, Arg};
use serde::Deserialize;
use std::error::Error;
use std::fs::File;
use std::path::Path;
use csv::ReaderBuilder;

#[derive(Debug, Deserialize)]
struct Employee {
    education: String,
    experience: u32,
    location: String,
    job_title: String,
    age: u32,
    gender: String,
    salary: f64,
}
fn read_data_from_csv(file_path: &str) -> Result<Vec<Employee>, Box<dyn Error>> {
    let file = File::open(file_path)?;
    let mut rdr = ReaderBuilder::new().from_reader(file);


    let mut employees = Vec::new();
    for result in rdr.records() { 
        let record = result?;   
        let employee = Employee {
            education: record[0].to_string(),
            experience: record[1].parse::<u32>()?,
            location: record[2].to_string(),
            job_title: record[3].to_string(),
            age: record[4].parse::<u32>()?,
            gender: record[5].to_string(),
            salary: record[6].parse::<f64>()?,
        };
        employees.push(employee);
    }

    Ok(employees)
}


fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("Salary Analyzer")
        .version("1.0")
        .author("Mutian")
        .about("Analyzes employee data")
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("file")
                .value_name("FILE")
    
                .takes_value(true),
        )
        .get_matches();

    let file_path = matches.value_of("file").unwrap_or("salary_prediction_data.csv");

    let employees = read_data_from_csv(file_path)?;
    let start_index = if employees.len() > 10 { employees.len() - 10 } else { 0 };
    for employee in &employees[start_index..] {
        println!("Salary: {:.2}", employee.salary);
    }
    // Perform analysis here
    let total_salary: f64 = employees.iter().map(|e| e.salary).sum();
    let average_salary = total_salary / employees.len() as f64;
    println!("Average Salary: {:.2}", average_salary);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Write;

    #[test]
    fn test_read_invalid_csv_data() {
        // Test invalid CSV data
        let invalid_csv_data = "High School,8,Urban,Manager,63,Male,84620.05\n\
                                PhD,11,Suburban,Director,59,Male,142591.26\n\
                                Bachelor,28,Suburban,Manager,61,Female\n\
                                High School,29,Rural,Director,45,Male,96834.67\n\
                                PhD,25,Urban,Analyst,26,Female,132157.79\n\
                                PhD,19,Rural,Director,27,Female,156312.94\n\
                                PhD,4,Rural,Director,60,Female,130567.65\n\
                                PhD,13,Suburban,Director,49,Female,148707.74\n\
                                Bachelor,20,Urban,Engineer,25,Female,95945.28\n\
                                PhD,14,Urban,Analyst,58,Female,133339.39";
        let mut temp_file_invalid = tempfile::NamedTempFile::new().unwrap();
        write!(temp_file_invalid, "{}", invalid_csv_data).unwrap();
        let invalid_file_path = temp_file_invalid.path().to_str().unwrap();
        let result_invalid = read_data_from_csv(invalid_file_path);
        assert!(result_invalid.is_err());
    }

    #[test]
    fn test_read_non_existent_file() {
        // Test non-existent file
        let non_existent_file_path = "/path/to/non_existent_file.csv";
        let result_non_existent = read_data_from_csv(non_existent_file_path);
        assert!(result_non_existent.is_err());
    }

   
}
